package cn.torna.dao.mapper;

import cn.torna.dao.entity.MsModuleConfig;
import com.gitee.fastmybatis.core.mapper.CrudMapper;

/**
 * @author thc
 */
public interface MsModuleConfigMapper extends CrudMapper<MsModuleConfig, Long> {

}
